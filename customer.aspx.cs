// All Rights Reserved.
// 
// Contributor(s): _______. 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Autofac;
using Nop.Core.Data;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using System.Data.Entity.Core.Objects;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.SqlClient;


namespace NopSolutions.NopCommerce.Web
{
public partial class customer : Page
{

 
public void Page_Load()
{
    string[,] list = new string[5000,10];


var dataSettingsManager1 = new DataSettingsManager();
var dataProviderSettings1 = dataSettingsManager1.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings.txt"));
System.Data.SqlClient.SqlConnection con1 = new System.Data.SqlClient.SqlConnection(dataProviderSettings1.DataConnectionString);
 	
	con1.Open();
    int i = 0;
   		
    try
    {
        System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(@"SELECT top 1 Id,Email FROM [bivoshop2018].[dbo].[Customer]  where Active=1 and Deleted=0 and Email !='NULL' order by [CreatedOnUtc] desc", con1);
	using (System.Data.SqlClient.SqlDataReader dr = command.ExecuteReader())
    {
	while (dr.Read())
    	{
            //list.Add(dr["Password"].ToString(), dr["PasswordSalt"].ToString(), dr["Email"].ToString(), dr["Id"].ToString());

            list[i, 0] = (dr["Id"].ToString());//Id
            list[i, 1] = (dr["Email"].ToString());//Email
 
            i++;

        }
                      
    }
	}
	catch {  }


   for (int j = 0; j < i; j++)
    {
        string Password= "", PasswordSalt="";
        try
        {
            System.Data.SqlClient.SqlCommand command2 = new System.Data.SqlClient.SqlCommand(@"SELECT top 1 Password,PasswordSalt FROM [bivoshop2018].[dbo].[CustomerPassword] where ([CustomerId]='" + list[j, 0] + "' ) order by [CreatedOnUtc] desc ", con1);
            using (System.Data.SqlClient.SqlDataReader dr2 = command2.ExecuteReader())
            {
                while (dr2.Read())
                {
                    //list.Add(dr["Password"].ToString(), dr["PasswordSalt"].ToString(), dr["Email"].ToString(), dr["Id"].ToString());

                    Password = (dr2[0].ToString());//Password
                    PasswordSalt = (dr2[1].ToString());//PasswordSalt

                }

            }
        }
        catch { }


 			
    string FN="",LN="",DB="";
    	
            try {

                System.Data.SqlClient.SqlCommand command1 = new System.Data.SqlClient.SqlCommand(@"select Value as firstname From GenericAttribute 
                            WHERE ([Key]='FirstName') and  (EntityId =  '" + list[j,0] + "' ) ", con1);
                using (System.Data.SqlClient.SqlDataReader dr1 = command1.ExecuteReader())
                {
                    if (dr1.Read() != null)
                    {
                        FN = dr1["firstname"].ToString();

                    }
                }
                System.Data.SqlClient.SqlCommand command2 = new System.Data.SqlClient.SqlCommand(@"select Value as lastname From GenericAttribute 
                            WHERE ([Key]='LastName') and  (EntityId =  '" + list[j, 0] + "' ) ", con1);
                using (System.Data.SqlClient.SqlDataReader dr2 = command2.ExecuteReader())
                {
                    if (dr2.Read() != null)
                    {
                        LN = dr2["lastname"].ToString();

                    }
                }
                System.Data.SqlClient.SqlCommand command3 = new System.Data.SqlClient.SqlCommand(@"select Value as Datebirth From GenericAttribute 
                            WHERE ([Key]='Datebirth') and  (EntityId =  '" + list[j, 0] + "' ) ", con1);
                using (System.Data.SqlClient.SqlDataReader dr3 = command2.ExecuteReader())
                {
                    if (dr3.Read() != null)
                    {
                        DB = dr3["Datebirth"].ToString();

                    }
                }
            }
            catch
                {}

//Response.Redirect("https://www.bivolino.eu/id=" + list[j, 1]);

int CustomerId = 0;
 var dataSettingsManager = new DataSettingsManager();
            var dataProviderSettings = dataSettingsManager.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings2.txt"));

            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(dataProviderSettings.DataConnectionString);
            using (con)
            {
                con.Open();
                try
                {

            SqlCommand cmd = new SqlCommand(@"INSERT INTO [Bivolino3D].[dbo].[Nop_Customer](CustomerGUID,Email,PasswordHash,SaltKey,AffiliateID,BillingAddressID,ShippingAddressID,LastPaymentMethodID,LastAppliedCouponCode,LanguageID,CurrencyID,IsAdmin,Active, Deleted, RegistrationDate) VALUES (@CustomerGUID,@Email,@PasswordHash,@SaltKey,1,'','','','',2,4,0,'True','False',@RegistrationDate)", con);

            cmd.Parameters.Add("@CustomerGUID", SqlDbType.UniqueIdentifier); String GuidVal = Guid.NewGuid().ToString();
            cmd.Parameters[0].Value = new System.Data.SqlTypes.SqlGuid(GuidVal);
            cmd.Parameters.Add("@Email", SqlDbType.VarChar); cmd.Parameters[1].Value = list[j, 1];
            cmd.Parameters.Add("@PasswordHash", SqlDbType.VarChar, 40); cmd.Parameters[2].Value = Password;
            cmd.Parameters.Add("@SaltKey", SqlDbType.VarChar); cmd.Parameters[3].Value = PasswordSalt;
            //cmd.Parameters.Add("@LanguageID", SqlDbType.Int); cmd.Parameters[4].Value = langid;
            cmd.Parameters.Add("@RegistrationDate", SqlDbType.DateTime); cmd.Parameters[4].Value = DateTime.Now;

            cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                
            }
				try
                {
                
                System.Data.SqlClient.SqlCommand command22 = new System.Data.SqlClient.SqlCommand(@"select top 1 [CustomerID] as CustomerId From [Bivolino3D].[dbo].[Nop_Customer] 
                            order by [RegistrationDate] desc", con);
                using (System.Data.SqlClient.SqlDataReader dr22 = command22.ExecuteReader())
                {
                    if (dr22.Read() != null)
                    {
                        CustomerId = int.Parse(dr22["CustomerId"].ToString());

                    }
                }
				}
                catch (Exception ex)
                {

                }


                try
                {

                    SqlCommand cmd1 = new SqlCommand(@"INSERT INTO [Bivolino3D].[dbo].[Nop_CustomerAttribute](CustomerId,[Key],Value) VALUES (@CustomerId,@Key,@Value)", con);
                    cmd1.Parameters.Add("@CustomerId", SqlDbType.Int); cmd1.Parameters[0].Value = CustomerId;
                    cmd1.Parameters.Add("@Key", SqlDbType.VarChar); cmd1.Parameters[1].Value = "FirstName";
                    cmd1.Parameters.Add("@Value", SqlDbType.VarChar); cmd1.Parameters[2].Value = FN;


                    cmd1.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                }

                try
                {

                    SqlCommand cmd2 = new SqlCommand(@"INSERT INTO [Bivolino3D].[dbo].[Nop_CustomerAttribute](CustomerId,[Key],Value) VALUES (@CustomerId,@Key,@Value)", con);
                    cmd2.Parameters.Add("@CustomerId", SqlDbType.Int); cmd2.Parameters[0].Value = CustomerId;
                    cmd2.Parameters.Add("@Key", SqlDbType.VarChar); cmd2.Parameters[1].Value = "LastName";
                    cmd2.Parameters.Add("@Value", SqlDbType.VarChar); cmd2.Parameters[2].Value = LN;


                    cmd2.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                }
                try
                {

                    SqlCommand cmd3 = new SqlCommand(@"INSERT INTO [Bivolino3D].[dbo].[Nop_CustomerAttribute](CustomerId,[Key],Value) VALUES (@CustomerId,@Key,@Value)", con);
                    cmd3.Parameters.Add("@CustomerId", SqlDbType.Int); cmd3.Parameters[0].Value = CustomerId;
                    cmd3.Parameters.Add("@Key", SqlDbType.VarChar); cmd3.Parameters[1].Value = "DateOfBirth";
                    cmd3.Parameters.Add("@Value", SqlDbType.VarChar); cmd3.Parameters[2].Value = DB;
                    cmd3.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                }
                try
                {

                    SqlCommand cmd4 = new SqlCommand(@"INSERT INTO [Bivolino3D].[dbo].[Nop_CustomerAttribute](CustomerId,[Key],Value) VALUES (@CustomerId,@Key,@Value)", con);
                    cmd4.Parameters.Add("@CustomerId", SqlDbType.Int); cmd4.Parameters[0].Value = CustomerId;
                    cmd4.Parameters.Add("@Key", SqlDbType.VarChar); cmd4.Parameters[1].Value = "Email";
                    cmd4.Parameters.Add("@Value", SqlDbType.VarChar); cmd4.Parameters[2].Value = list[j, 1];
                    cmd4.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                }
            
            finally
            {
                con.Close();
                //con1.Close();
            }
            }
          
            }
Response.Redirect("https://www.bivolino.com");           
    		                        
}

}
}
