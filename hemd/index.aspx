﻿<%@ Page Language="C#" %>
<%

	//! Tradetracker Redirect-Page.

	// Set domain name on which the redirect-page runs, WITHOUT "www.".
    string domainName = "bivolino.com";

	// Set the P3P compact policy.
	Response.AddHeader("P3P", "CP=\"ALL PUR DSP CUR ADMi DEVi CONi OUR COR IND\"");

	// Define parameters.
	string campaignID = "";
	string materialID = "";
	string affiliateID = "";
	string redirectURL = "";
	string reference = "";

	bool canRedirect = true;

	// Set parameters.
	if (Request.QueryString["campaignID"] != null)
	{
		campaignID = Request.QueryString["campaignID"];
		materialID = Request.QueryString["materialID"];
		affiliateID = Request.QueryString["affiliateID"];
		redirectURL = Request.QueryString["redirectURL"];
	}
	else if (Request.QueryString["tt"] != null)
	{
		string[] trackingData = Request.QueryString["tt"].Split('_');

		campaignID = trackingData.Length >= 1 ? trackingData[0] : "";
		materialID = trackingData.Length >= 2 ? trackingData[1] : "";
		affiliateID = trackingData.Length >= 3 ? trackingData[2] : "";
		reference = trackingData.Length >= 4 ? trackingData[3] : "";

		redirectURL = Request.QueryString["r"];
	}
	else
		canRedirect = false;

	if (canRedirect)
	{
		// Calculate MD5 checksum.
		string checkSum = FormsAuthentication.HashPasswordForStoringInConfigFile("CHK_" + campaignID + "::" + materialID + "::" + affiliateID + "::" + reference, "MD5");

		// Calculate unix time stamp.
		int timeStamp = (int) (DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;

		// Set session/cookie arguments.
		string cookieName = "TT2_" + campaignID;
		string cookieValue = materialID + "::" + affiliateID + "::" + reference + "::" + checkSum + "::" + timeStamp;

		// Set session data.
		Session.Add(cookieName, cookieValue);

		// Create tracking cookie.
		Response.Cookies[cookieName].Value = cookieValue;
		Response.Cookies[cookieName].Expires = DateTime.Now.AddDays(365);
		Response.Cookies[cookieName].Path = "/";

		if (domainName != "")
			Response.Cookies[cookieName].Domain = "." + domainName;

		// Set track-back URL.
		string trackBackURL = "http://tc.tradetracker.net/?c=" + campaignID + "&m=" + materialID + "&a=" + affiliateID + "&r=" + HttpUtility.UrlEncode(reference) + "&u=" + HttpUtility.UrlEncode(redirectURL);

		// Redirect to TradeTracker.
		Response.Status = "301 Moved Permanently";
		Response.AddHeader("Location", trackBackURL);
	}

%>