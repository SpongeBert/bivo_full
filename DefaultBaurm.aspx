<%@ Page Language="C#" MasterPageFile="~/mainHOMEBAUR.master" AutoEventWireup="true" CodeFile="DefaultBaurm.aspx.cs" Inherits="NopSolutions.NopCommerce.Web.DefaultBaurmPage" Title="Untitled Page" %>

<%@ Register TagPrefix="nopCommerce" TagName="Header" Src="~/Modules/Header.ascx" %>
<%@ Register TagPrefix="nopCommerce" TagName="AffiliateModule" Src="~/Modules/AffiliateModule.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" Runat="Server">

    
    <div id="op" style="display:none"><nopCommerce:AffiliateModule ID="AffOption" runat="server" Visible="false" /></div> 
    <div id="Baurm_page">
        <div id="Baurm_page_center">
         
           <div id="Baurm_top_part">
           <img src="https://www.designer.bivolino.com/baurm/images/Baur_Landing-Page-2016_model_03.jpg" style="width:982px;height:402px;"/>
             <%--<div id="Baurm_left_part">
                 <img id="text_t01" alt="Pers�nliches Mabhemd" src="http://www.bivolino.com/Baurm/images/baur_Landing-Page-2011_03.png" /><br />
                 <p>In nur 4 Schritten k&ouml;nnen Sie sich einfach und bequem ein Hemd nach Ma&szlig; kreieren, das Ihnen steht und perfekt passt. Sie haben mehrere M&ouml;glichkeiten, Ihre individuellen W&#252nsche zu verwirklichen: Stoffqualit&#228;t, Kragenform,  Manschetten, Tasche, Saum,  Monogramm und vieles mehr.</p><br />
                 <p>Preise ab &#128;59,- Die Hemden werden  innerhalb von 21 Tagen geliefert.</p><br />
                 <p>W&#228;hlen Sie zwischen geradem Schnitt, leichter Taillierung und taillierter Schnittform. Damit Sie Ihr Ma&szlig;hemd genau in Ihrer Gr&ouml;&szlig;e und Passform erhalten, wenden wir biometrische Technologie an, die Ihre Altersangabe und Ihre K&ouml;rperma&szlig;e einbezieht.</p>
             </div>--%>
             <%--<div id="Baurm_midium_part">
                 <img alt="Shoose ein T-Shirt-Stil" src="http://www.bivolino.com/Baurm/images/baur_Landing-Page-2011_07.png" />
             </div>--%>
             <div id="Baurm_right_part">
                 <%--<a href="http://www.bivolino.com/s_mobile/config.aspx?CID=65&affid=10064&cur=1&lid=4" target="_self"><img id="Baurm_model" alt="modell" src="http://www.bivolino.com/img_promo/<%=H_de_Mnq %>" /></a>
                 --%>
                 <div id="Baurm_button">
                   <a id="Baurm_button01" href="https://www.designer.bivolino.com/s_mobile/config.aspx?CID=65&affid=10064&cur=1&lid=4" target="_self"><img alt="Los geht's" src="https://www.designer.bivolino.com/Baurm/images/baur_Landing-Page-2011_13.png" /></a>
                 </div>
                
                 <div id="patch_Baurm" style="margin-top:-80px;margin-left:200px;width:90px;height:90px;z-index:20;">
                  <%if (H_d_patch == "")  { %>  
                    <img alt="" src="https://www.designer.bivolino.com/img_promo/blank.jpg" />
                      <%}    else  { %>
                    <img alt="F�rderung - Gleichgewicht" src="https://www.designer.bivolino.com/img_promo/<%=H_d_patch %>" />
            <%} %>
             </div>
             </div>
             <div class="clearBoth"> </div>
          </div> 
          
          <div id="Baurm_down_part">
             <div id="Baurm_foursteps">
                <div class="Baurm_steps" id="Baurm_step01">
                  
                  <img alt="Blitzschnell zum Unikat" src="https://www.designer.bivolino.com/baurm/images/01.jpg" /><br />
                  <h3>Blitzschnell zum Unikat!</h3>
                  <p>Design ausw&#228;hlen, Stoff aussuchen, Extras hinzuf&#252gen und  Ma&szlig;e eingeben � fertig ist Ihr pers&#246;nliches Unikat!</p>
                </div>
                <div class="Baurm_steps" id="Baurm_step02">
                  
                  <img alt="Lieferung in 3 nur Wochen" src="https://www.designer.bivolino.com/baurm/images/02.jpg" /><br />
                  <h3>Lieferung in nur 21 Tagen!</h3>
                  <p>Ma&szlig;arbeit braucht etwas Zeit.  Gro&szlig;e Sorgfalt und  z&#252gige Abwicklung � schon nach 3 Wochen hei&szlig;t es "Anprobe".</p>
                </div>
                <div class="Baurm_steps" id="Baurm_step03">
                  
                  <img alt="50 Stoffe zu Ihrer Auswahl" src="https://www.designer.bivolino.com/baurm/images/03.jpg" /><br />
                  <h3>30 Stoffe zu Ihrer Auswahl!</h3>
                  <p>Alles ist m&#246;glich! Ganz nach Trageanlass und pers&#246;nlichen Vorlieben w&#228;hlen Sie Stoff-Qualit&#228;t, Farbe und Design aus.</p>
                </div>
                <div class="Baurm_steps" id="Baurm_step04">
                  
                  <img alt="Keine R�cksendung" src="https://www.designer.bivolino.com/baurm/images/04.jpg" /><br />
                  <h3>Kostenlose R&#252cksendung!</h3>
                  <p>Ihr Hemd passt oder sitzt nicht richtig? Sie erhalten ein neues. Kostenlos!<br /><a href="https://www.designer.bivolino.com/baurm/d_beschwerde.htm">Beschwerde</a></p>
                </div>  
             </div>
          </div>
        </div>
       

        
</div>
    
</asp:Content>

