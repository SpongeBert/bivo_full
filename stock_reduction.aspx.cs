// All Rights Reserved.
// 
// Contributor(s): _______. 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Autofac;
using Nop.Core.Data;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using System.Data.Entity.Core.Objects;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.SqlClient;


namespace NopSolutions.NopCommerce.Web
{
public partial class configPage2 : Page
{

 
public void Page_Load()
{
string[] stringSku = new string[10];
string[] stringQuantity = new string[10];
int i=0;
var dataSettingsManager1 = new DataSettingsManager();
var dataProviderSettings1 = dataSettingsManager1.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings.txt"));
System.Data.SqlClient.SqlConnection con1 = new System.Data.SqlClient.SqlConnection(dataProviderSettings1.DataConnectionString);
 	using (con1)
    {
	con1.Open();
     try
    {
    string id=Request.QueryString["id"].ToString();
System.Data.SqlClient.SqlCommand command1 = new System.Data.SqlClient.SqlCommand(@"SELECT b.Sku,a.Quantity FROM OrderItem as a INNER JOIN Product as b ON a.ProductId=b.Id WHERE a.OrderId="+id+"",con1);
    using (System.Data.SqlClient.SqlDataReader dr1 = command1.ExecuteReader())
    {
    
	while (dr1.Read())
    	{

         	stringSku[i] = dr1["Sku"].ToString();
 
            stringQuantity[i] = dr1["Quantity"].ToString();
			
         
	var dataSettingsManager = new DataSettingsManager();
    var dataProviderSettings = dataSettingsManager.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings2.txt"));
    System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(dataProviderSettings.DataConnectionString);
         

			con.Open();
    		int fabric_id = 0;
	
			//for(int j=0;j < stringSku.Length;j++)
		//	{
                try
                {
			//	Response.Write(stringSku[j].ToString());
                    System.Data.SqlClient.SqlCommand command2 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select cast((xmldoc.query('/order/fab_/text()')) as varchar(max)))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr2 = command2.ExecuteReader())
                    {

                        if (dr2.Read())
                        {
                            fabric_id = int.Parse(dr2["FabID"].ToString());

                      
                        }

                    }
				}
         
            catch {  }
          
          float laststock=0;
          System.Data.SqlClient.SqlCommand command3 = new System.Data.SqlClient.SqlCommand(@"SELECT TOP (1) PhysicalStock as stock FROM 
          OFMS_FabricStock WHERE     (fabric_id = " + fabric_id + ") AND (ActionID <> 1) ORDER BY Date DESC", con);
           using (System.Data.SqlClient.SqlDataReader dr3 = command3.ExecuteReader())
                    {
                       if (dr3.Read())
                        {
                            laststock = float.Parse(dr3["stock"].ToString());
// Response.Write(laststock.ToString());
                        }

                 dr3.Close();
			
            //int idaction=5;
            if(stringQuantity[i] != null && laststock>0)
			{
            int qtity=int.Parse(stringQuantity[i]);
		
            float metres=float.Parse((1.75 * qtity).ToString());
       
            float Decreased=float.Parse((-metres).ToString());
            laststock=laststock+Decreased;

            SqlCommand command4 = new SqlCommand(@"INSERT INTO OFMS_FabricStock (AffiliateID,fabric_id, Date, ActionID, Ordered, SalesOnlineQuantity, SalesOnlineMetres, Delivered, Decreased, PhysicalStock) VALUES (1,@fabric_id,@Date,5,NULL,@SalesOnlineQuantity,@SalesOnlineMetres,NULL,@Decreased,@PhysicalStock)",con);
            
            command4.Parameters.AddWithValue("@fabric_id",fabric_id);
            command4.Parameters.AddWithValue("@Date",DateTime.Now);
           // command4.Parameters.AddWithValue("@ActionID",idaction.ToString());
            command4.Parameters.AddWithValue("@SalesOnlineQuantity",qtity);
            command4.Parameters.AddWithValue("@SalesOnlineMetres",metres);
            command4.Parameters.AddWithValue("@Decreased",Decreased);
            command4.Parameters.AddWithValue("@PhysicalStock",laststock);
            command4.ExecuteNonQuery();
	       }
					}
			
		
    		
			int contrast_id=0;
			
				
                    System.Data.SqlClient.SqlCommand command33 = new System.Data.SqlClient.SqlCommand(@"select '1'+SUBSTRING (cast((xmldoc.query('/order/con_61/text()')) as varchar(max)),3,4) as FabID
                            FROM Sel_Selectors  AS GAL
                            WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr33 = command33.ExecuteReader())
                    {
                        if (dr33.Read())
                        {
                            contrast_id = int.Parse(dr33["FabID"].ToString());

                        }   
                    }
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command44 = new System.Data.SqlClient.SqlCommand(@"select '1'+SUBSTRING (cast((xmldoc.query('/order/con_62/text()')) as varchar(max)),3,4) as FabID
                            FROM Sel_Selectors  AS GAL
                            WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr44 = command44.ExecuteReader())
                    {
                        if (dr44.Read())
                        {
                            contrast_id = int.Parse(dr44["FabID"].ToString());

                        }   
                    }
				}

				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command55 = new System.Data.SqlClient.SqlCommand(@"select '1'+SUBSTRING (cast((xmldoc.query('/order/con_58/text()')) as varchar(max)),3,4) as FabID
                            FROM Sel_Selectors  AS GAL 
                            WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr55 = command55.ExecuteReader())
                    {
                        if (dr55.Read())
                        {
                            contrast_id = int.Parse(dr55["FabID"].ToString());

                        }   
                    }
				}
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command66 = new System.Data.SqlClient.SqlCommand(@"select '1'+SUBSTRING (cast((xmldoc.query('/order/con_87/text()')) as varchar(max)),3,4) as FabID
                            FROM Sel_Selectors  AS GAL 
                            WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr66 = command66.ExecuteReader())
                    {
                        if (dr66.Read())
                        {
                            contrast_id = int.Parse(dr66["FabID"].ToString());

                        }   
                    }
				}
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command77 = new System.Data.SqlClient.SqlCommand(@"select '1'+SUBSTRING (cast((xmldoc.query('/order/con_54/text()')) as varchar(max)),3,4) as FabID
                            FROM Sel_Selectors  AS GAL 
							WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr77 = command77.ExecuteReader())
                    {
                        if (dr77.Read())
                        {
                            contrast_id = int.Parse(dr77["FabID"].ToString());

                        }   
                    }
				}
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command88 = new System.Data.SqlClient.SqlCommand(@"select '1'+SUBSTRING (cast((xmldoc.query('/order/con_91/text()')) as varchar(max)),3,4) as FabID
                            FROM Sel_Selectors  AS GAL 
                            WHERE  (GAL.selector_id =  '" + stringSku[i] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr88 = command88.ExecuteReader())
                    {
                        if (dr88.Read())
                        {
                            contrast_id = int.Parse(dr88["FabID"].ToString());

                        }   
                    }
				}

		  float laststockcontrast=0;
          System.Data.SqlClient.SqlCommand command99 = new System.Data.SqlClient.SqlCommand(@"SELECT TOP (1) PhysicalStock as stock FROM 
          OFMS_FabricStock WHERE     (fabric_id = " + contrast_id + ") AND (ActionID <> 1) ORDER BY Date DESC", con);
          using (System.Data.SqlClient.SqlDataReader dr99 = command99.ExecuteReader())
                    {
                       if (dr99.Read())
                        {
                            laststockcontrast = float.Parse(dr99["stock"].ToString());
			// Response.Write(laststock.ToString());
                        }

                 dr99.Close();
			
            //int idaction1=6;
            if(stringQuantity[i] != null && contrast_id !=0 && contrast_id !=1)
			{
            int qtity1=int.Parse(stringQuantity[i]);
		
            float metrescontrast=float.Parse((0.15 * qtity1).ToString());
       
            float Decreasedcontrast=float.Parse((-metrescontrast).ToString());
			laststockcontrast=laststockcontrast+Decreasedcontrast;

            SqlCommand command100 = new SqlCommand(@"INSERT INTO OFMS_FabricStock (AffiliateID,fabric_id, Date, ActionID, Ordered, SalesOnlineQuantity, SalesOnlineMetres, Delivered, Decreased, PhysicalStock) VALUES (1,@fabric_id,@Date,6,NULL,@SalesOnlineQuantity,@SalesOnlineMetres,NULL,@Decreased,@PhysicalStock)",con);
            
            command100.Parameters.AddWithValue("@fabric_id",contrast_id);
            command100.Parameters.AddWithValue("@Date",DateTime.Now);
           
            command100.Parameters.AddWithValue("@SalesOnlineQuantity",qtity1);
            command100.Parameters.AddWithValue("@SalesOnlineMetres",metrescontrast);
            command100.Parameters.AddWithValue("@Decreased",Decreasedcontrast);
            command100.Parameters.AddWithValue("@PhysicalStock",laststockcontrast);
            command100.ExecuteNonQuery();
	       }
			}
			 				

i++;
            
		
        }
                      
    }
	}
	finally {  }
    }

Response.Redirect("https://www.bivolino.com/stock.aspx");                         
}

}
}
