// All Rights Reserved.
// 
// Contributor(s): _______. 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Autofac;
using Nop.Core.Data;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using System.Data.Entity.Core.Objects;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.SqlClient;


namespace NopSolutions.NopCommerce.Web
{
public partial class price : Page
{

 
public void Page_Load()
{
System.Threading.Thread.Sleep(4000);

string price1=Request.QueryString["p"].ToString();
if (Request.QueryString["cur"].ToString() == "2")
{
price1= (Math.Round(Decimal.Parse(price1) * Decimal.Parse("1.1084"))).ToString(); 
}
        
var dataSettingsManager1 = new DataSettingsManager();
var dataProviderSettings1 = dataSettingsManager1.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings.txt"));

System.Data.SqlClient.SqlConnection con1 = new System.Data.SqlClient.SqlConnection(dataProviderSettings1.DataConnectionString);
 	using (con1)
    {
	con1.Open();
                
			
    try
    {
	System.Data.SqlClient.SqlCommand command1 = new System.Data.SqlClient.SqlCommand(@"UPDATE [bivoshop2018].[dbo].[ShoppingCartItem] SET CustomerEnteredPrice=@CustomerEnteredPrice,AttributesXml=@AttributesXml WHERE ProductId = '257' and [Id] = (SELECT MAX([Id]) FROM [bivoshop2018].[dbo].[ShoppingCartItem])",con1);
                  	command1.Parameters.AddWithValue("@CustomerEnteredPrice", price1 );
                    command1.Parameters.AddWithValue("@AttributesXml", "<Attributes><ProductAttribute ID="+48+"><ProductAttributeValue><Value>"+Request.QueryString["SELID"]+"</Value></ProductAttributeValue></ProductAttribute></Attributes>");

                    command1.ExecuteNonQuery();


                   System.Data.SqlClient.SqlCommand command2 = new System.Data.SqlClient.SqlCommand(@"UPDATE [bivoshop2018].[dbo].[Product] SET Price=@Price WHERE Id = '257'",con1);
                
                  	command2.Parameters.AddWithValue("@Price",price1);
                  

                   command2.ExecuteNonQuery();
}

	catch {  }
}   
              
Response.Write("<script>window.top.location.href='https://www.bivolino.com/cart';</script>");


}

}
}