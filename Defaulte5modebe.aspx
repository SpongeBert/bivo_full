﻿<%@ Page Language="C#" MasterPageFile="~/mainHOME.master" AutoEventWireup="true" CodeFile="Defaulte5modebe.aspx.cs" Inherits="NopSolutions.NopCommerce.Web.Defaulte5modebePage" Title="Untitled Page" %>



<asp:Content ID="Content1" ContentPlaceHolderID="cph1" Runat="Server">

    <div id="myPage_e5mode">
<div class="Pagecenter_e5mode">



      <div id="breadcrumbs">
		  <%--<p>Welcome to our specialized website: www.e5mode.bivolino.com</p>--%>
	  </div><!-- einde div breadcrumbs -->
    
    
      <div id="up_page_e5mode">
         
         <div id="text_description_e5mode"><img alt="" src="<%=ResolveUrl("~/")%>e5modebe/images/teaser_e5mode.png" /></div>
         <%--<div id="logo_Bivo_HandM"><img alt="" src="<%=ResolveUrl("~/")%>HighandMighty/images/logo_bivolino_HandM.png" /></div>--%>         
         
         <div id="patch_HandM" style="width:90px;height:90px;float:left !important;margin-top:-280px;position:relative; z-index:20;">
           <%if (H_fr_patch == "")
             { %>  
           <img alt="" src="img_promo/blank.jpg" />
            <%}    else  { %>
             <img alt="" src="img_promo/<%= H_fr_patch %>" />
            <%} %>
         </div>
         <div id="model__e5mode">
             <%if (H_fr_Mnq == "")  { %>
             
              <img style="float:left;margin-top:-283px;margin-left:-35px;" alt="Bivolino" src="<%=ResolveUrl("~/")%>img_promo/teaser_e5mode_model.png" />
             <%} else  { %> 
              <img style="float:left;margin-top:-283px;margin-left:-35px;" alt="" src="img_promo/<%=H_fr_Mnq %>" />           
            <%} %>
         </div>
         <div id="button_design_gallery_e5mode">
           <a href="confpage.aspx?CID=125&affid=10186&conflid=3"><img alt="Créer votre chemise" src="<%=ResolveUrl("~/")%>e5modebe/images/button_conf_e5mode.png" /></a>
         </div>
      </div><%--up_page_HandM--%>
      
      <div id="down_page_e5mode">
         <h3>Modèles populaires:</h3>
         <div id="gallery_e5mode">
                <div class="shirt_e5mode" id="shirt01_HandM"><a href="confpage.aspx?CID=125&affid=10186&conflid=1" target="_self"><img alt="" src="<%=ResolveUrl("~/")%>e5mode/images/Shirt-1.png" /></a></div>
                <div class="shirt_e5mode" id="shirt02_HandM"><a href="confpage.aspx?CID=125&affid=10186&conflid=1" target="_self"><img alt="" src="<%=ResolveUrl("~/")%>e5mode/images/Shirt-2.png" /></a></div>
                <div class="shirt_e5mode" id="shirt03_HandM"><a href="confpage.aspx?CID=125&affid=10186&conflid=1" target="_self"><img alt="" src="<%=ResolveUrl("~/")%>e5mode/images/Shirt-3.png" /></a></div>
                <div class="shirt_e5mode" id="shirt04_HandM"><a href="confpage.aspx?CID=125&affid=10186&conflid=1" target="_self"><img alt="" src="<%=ResolveUrl("~/")%>e5mode/images/Shirt-4.png" /></a></div>
                <div class="shirt_e5mode" id="shirt05_HandM"><a href="confpage.aspx?CID=125&affid=10186&conflid=1" target="_self"><img alt="" src="<%=ResolveUrl("~/")%>e5mode/images/Shirt-6.png" /></a></div> 
         </div>
         
         <div id="foursteps_e5mode">
                <h3>Créer votre propre chemise en 4 étapes:</h3>
		        <ul>
                <li id="steps1_e5mode"><img alt="1" src="<%=ResolveUrl("~/")%>e5modebe/images/e5mode_22.jpg" /><p>Faites votre choix parmi plus<br /> <span>de 75 tissus de qualité!</span></p></li> 
                <li id="steps2_e5mode"><img alt="2" src="<%=ResolveUrl("~/")%>e5modebe/images/e5mode_25.jpg" /><p>Choisissez le bord, les manchettes, la<br /><span>poche et la fermuture que vous préférez.</span></p></li>
                <li id="steps3_e5mode"><img alt="3" src="<%=ResolveUrl("~/")%>e5modebe/images/e5mode_27.jpg" /><p>Placez vos initiales sur l'un des<br /><span>10 endroits.</span></p></li>
                <li id="steps4_e5mode"><img alt="4" src="<%=ResolveUrl("~/")%>e5modebe/images/e5mode_29.jpg" /><p>La technologie des tailles vous donne<br /><span>toujours la bonne taille.</span></p></li>
                </ul>
         </div>
      </div>
        
         
        
 </div><!--------.Pagecenter--------->    
</div>
       
</asp:Content>


            
  

       
       
        



