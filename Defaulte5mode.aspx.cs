﻿//------------------------------------------------------------------------------
// The contents of this file are subject to the nopCommerce Public License Version 1.0 ("License"); you may not use this file except in compliance with the License.
// You may obtain a copy of the License at  http://www.nopCommerce.com/License.aspx. 
// 
// Software distributed under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. 
// See the License for the specific language governing rights and limitations under the License.
// 
// The Original Code is nopCommerce.
// The Initial Developer of the Original Code is NopSolutions.
// All Rights Reserved.
// 
// Contributor(s): _______. 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using NopSolutions.NopCommerce.Common;
using NopSolutions.NopCommerce.Common.Localization;
using NopSolutions.NopCommerce.Common.Payment;
using NopSolutions.NopCommerce.Common.Utils;
using myDatabase;
using System.Collections.Generic;
using NopSolutions.NopCommerce.Common.Configuration.Settings;
using NopSolutions.NopCommerce.Common.Directory;
namespace NopSolutions.NopCommerce.Web
{
    public partial class Defaulte5modePage : BaseNopPage
    {
        private string _gal1 = "";
        public string gal1
        { get { return _gal1; } set { _gal1 = value; } }

        private string _gal1sel = "";
        public string gal1sel
        { get { return _gal1sel; } set { _gal1sel = value; } }

        private string _gal2 = "";
        public string gal2
        { get { return _gal2; } set { _gal2 = value; } }

        private string _gal2sel = "";
        public string gal2sel
        { get { return _gal2sel; } set { _gal2sel = value; } }

        private string _gal3 = "";
        public string gal3
        { get { return _gal3; } set { _gal3 = value; } }

        private string _gal3sel = "";
        public string gal3sel
        { get { return _gal3sel; } set { _gal3sel = value; } }

        private string _gal4 = "";
        public string gal4
        { get { return _gal4; } set { _gal4 = value; } }

        private string _gal4sel = "";
        public string gal4sel
        { get { return _gal4sel; } set { _gal4sel = value; } }

        private string _gal5 = "";
        public string gal5
        { get { return _gal5; } set { _gal5 = value; } }

        private string _gal5sel = "";
        public string gal5sel
        { get { return _gal5sel; } set { _gal5sel = value; } }

        private string _H_nl_Mnq = "";
        public string H_nl_Mnq
        { get { return _H_nl_Mnq; } set { _H_nl_Mnq = value; } }

        private string _H_nl_patch = "";
        public string H_nl_patch
        { get { return _H_nl_patch; } set { _H_nl_patch = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            //PROMO BANNERS//////////////////////////////
            string voucherid = DB.getPromoVouchercode22(10178, 2);
            List<string> promodetails = new List<string>();
            if (string.IsNullOrEmpty(voucherid))
            {
                //promodetails = DB.getPromoDetails(10040);
                promodetails = DB.GetPromoDetailsNewStaticOct2011(10178, 2);
            }
            else
            {
                promodetails = DB.getPromoDetails22(int.Parse(voucherid), 2);
            }

            if (promodetails.Count > 0)
            {
                if (promodetails.Count != 8)
                {
                    if (!string.IsNullOrEmpty(promodetails[24].ToString()))
                    {
                        H_nl_Mnq = promodetails[24].ToString();
                    }
                    else H_nl_Mnq = "blank.jpg";

                    if (!string.IsNullOrEmpty(promodetails[8].ToString()))
                    {
                        H_nl_patch = promodetails[8].ToString();
                    }
                    else
                        H_nl_patch = "blank.jpg";
                }
                else
                {
                    /*
                     @Home_patch 2
                    ,@Home_Mnq  3
                    ,@Fab3DBanner  4
                     ,@shoppingBasketBanner 5
                     */
                    H_nl_Mnq = promodetails[3];
                    H_nl_patch = promodetails[2];
                }
            }
            else
            {
                H_nl_Mnq = "blank.jpg";
                H_nl_patch = "blank.jpg";
            }
            /////////////////////////////////////////////
        }
    }

}
