//------------------------------------------------------------------------------ Saoussen Automatic Generation OF files for affiliates 2011
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using NopSolutions.NopCommerce.Common;
using NopSolutions.NopCommerce.Common.Localization;
using NopSolutions.NopCommerce.Common.Payment;
using NopSolutions.NopCommerce.Common.Utils;
using myDatabase;
using System.Collections.Generic;
using NopSolutions.NopCommerce.Common.Configuration.Settings;
using NopSolutions.NopCommerce.Common.Directory;
using NopSolutions.NopCommerce.Common.Audit;
namespace NopSolutions.NopCommerce.Web
{
    public partial class DefaultBaurmPage : BaseNopPage 
    {
        private string _gal1 = "";
        public string gal1
        { get { return _gal1; } set { _gal1 = value; } }
        private string _gal1sel = "";
        public string gal1sel
        { get { return _gal1sel; } set { _gal1sel = value; } }
        private string _gal1nameshirt = "";
        public string gal1nameshirt
        { get { return _gal1nameshirt; } set { _gal1nameshirt = value; } }
        private string _gal1namecreator = "";
        public string gal1namecreator
        { get { return _gal1namecreator; } set { _gal1namecreator = value; } }
        private string _gal1price = "";
        public string gal1price
        { get { return _gal1price; } set { _gal1price = value; } }
        /// <summary>
        //////////////////////
        /// </summary>
        private string _gal2 = "";
        public string gal2
        { get { return _gal2; } set { _gal2 = value; } }
        private string _gal2sel = "";
        public string gal2sel
        { get { return _gal2sel; } set { _gal2sel = value; } }
        private string _gal2nameshirt = "";
        public string gal2nameshirt
        { get { return _gal2nameshirt; } set { _gal2nameshirt = value; } }
        private string _gal2namecreator = "";
        public string gal2namecreator
        { get { return _gal2namecreator; } set { _gal2namecreator = value; } }
        private string _gal2price = "";
        public string gal2price
        { get { return _gal2price; } set { _gal2price = value; } }
        /// <summary>
        /// ///////////////////
        /// </summary>
        private string _gal3 = "";
        public string gal3
        { get { return _gal3; } set { _gal3 = value; } }
        private string _gal3sel = "";
        public string gal3sel
        { get { return _gal3sel; } set { _gal3sel = value; } }
        private string _gal3nameshirt = "";
        public string gal3nameshirt
        { get { return _gal3nameshirt; } set { _gal3nameshirt = value; } }
        private string _gal3namecreator = "";
        public string gal3namecreator
        { get { return _gal3namecreator; } set { _gal3namecreator = value; } }
        private string _gal3price = "";
        public string gal3price
        { get { return _gal3price; } set { _gal3price = value; } }
        /// <summary>
        /// ///////////////////
        /// </summary>
        private string _gal4 = "";
        public string gal4
        { get { return _gal4; } set { _gal4 = value; } }
        private string _gal4sel = "";
        public string gal4sel
        { get { return _gal4sel; } set { _gal4sel = value; } }
        private string _gal4nameshirt = "";
        public string gal4nameshirt
        { get { return _gal4nameshirt; } set { _gal4nameshirt = value; } }
        private string _gal4namecreator = "";
        public string gal4namecreator
        { get { return _gal4namecreator; } set { _gal4namecreator = value; } }
        private string _gal4price = "";
        public string gal4price
        { get { return _gal4price; } set { _gal4price = value; } }
        /// <summary>
        /// ///////////////////
        /// </summary>
        private string _gal5 = "";
        public string gal5
        { get { return _gal5; } set { _gal5 = value; } }
        private string _gal5sel = "";
        public string gal5sel
        { get { return _gal5sel; } set { _gal5sel = value; } }
        private string _gal5nameshirt = "";
        public string gal5nameshirt
        { get { return _gal5nameshirt; } set { _gal5nameshirt = value; } }
        private string _gal5namecreator = "";
        public string gal5namecreator
        { get { return _gal5namecreator; } set { _gal5namecreator = value; } }
        private string _gal5price = "";
        public string gal5price
        { get { return _gal5price; } set { _gal5price = value; } }
        private string _H_de_Mnq = "";
        public string H_de_Mnq
        { get { return _H_de_Mnq; } set { _H_de_Mnq = value; } }
        private string _H_d_patch = "";
        public string H_d_patch
        { get { return _H_d_patch; } set { _H_d_patch = value; } }
        protected string GetImageUrl()
        {
            string url = "~/";
            return ResolveClientUrl(url);
        }
        public bool SetCookie(string cookiename, string cookievalue, int iDaysToExpire)
        {
            try
            {
                Response.Cookies[cookiename].Expires = DateTime.Now.AddDays(-1);
                HttpCookie cookie = new HttpCookie(cookiename);
                cookie.Value = cookievalue;
                cookie.Expires = DateTime.Now.AddDays(iDaysToExpire);
                Response.SetCookie(cookie);
            }
            catch (Exception e)
            { return false; }
            return true;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                CommonHelper.EnsureSSL();
            }
            //DB.MailAlert("url baur", Request.UrlReferrer.ToString());
            //DB.MailAlert("url baur", Request.UrlReferrer.ToString().Substring(45));

            if (Request.UrlReferrer != null && Request.UrlReferrer.ToString().Length > 44)
            {
                Session["BAURURL"] = Request.UrlReferrer.ToString().Substring(45);
                //DB.MailAlert("session baururl in defaultbaurm", Session["BAURURL"].ToString());
                LogManager.InsertLog(LogTypeEnum.OrderError, "session baururl in defaultbaurm", Session["BAURURL"].ToString());
            }
            else
            {
                Session["BAURURL"] = "leeg";
                //DB.MailAlert("session baururl in defaultbaurm", "leeg");
                LogManager.InsertLog(LogTypeEnum.OrderError, "session baururl in defaultbaurm LEEG ", "LEEG");
            }


            SetCookie("Nop.CustomerLanguage", "4", 100);
            NopContext.Current.WorkingLanguage.LanguageID = 4;
            //PROMO BANNERS//////////////////////////////
            string voucherid = DB.getPromoVouchercode22(10064, 4);
            List<string> promodetails = new List<string>();
            if (string.IsNullOrEmpty(voucherid))
            {
                //promodetails = DB.getPromoDetails(10040);
                promodetails = DB.GetPromoDetailsNewStaticOct2011(10064, 4);
            }
            else
            {
                promodetails = DB.getPromoDetails22(int.Parse(voucherid), 4);
            }

            if (promodetails.Count > 0)
            {
                if (promodetails.Count != 8)
                {
                    if (!string.IsNullOrEmpty(promodetails[27].ToString()))
                    {
                        H_de_Mnq = promodetails[27].ToString();
                    }
                    else H_de_Mnq = "blank.jpg";

                    if (!string.IsNullOrEmpty(promodetails[11].ToString()))
                    {
                        H_d_patch = promodetails[11].ToString();
                    }
                    else
                        H_d_patch = "blank.jpg";
                }
                else
                {
                    /*
                     @Home_patch 2
                    ,@Home_Mnq  3
                    ,@Fab3DBanner  4
                     ,@shoppingBasketBanner 5
                     */
                    H_de_Mnq = promodetails[3];
                    H_d_patch = promodetails[2];
                }
            }
            else
            {
                H_de_Mnq = "blank.jpg";
                H_d_patch = "blank.jpg";
            }
            //linkconf.HRef = SettingManager.GetSettingValue("Common.StoreURL") + "/bivo_daily/confpage.aspx?CID=42&affid=10010&conflid=2";
            //linkacc.HRef = SettingManager.GetSettingValue("Common.StoreURL") + "/bivo_daily/login.aspx?affid=10010";
            //linkacc2.HRef = SettingManager.GetSettingValue("Common.StoreURL") + "/bivo_daily/login.aspx?affid=10010";
            //conflink.HRef = SettingManager.GetSettingValue("Common.StoreURL") + "/bivo_daily/confpage.aspx?CID=42&affid=10010&conflid=2";
            //linkgal1.HRef = SettingManager.GetSettingValue("Common.StoreURL") + "/bivo_daily/galpage.aspx?CID=42&affid=10010&gallid=2";

        }
        protected void Submit_Click(object sender, EventArgs e)
        {
        }

        //public static string ResolveUrl(string originalUrl)
        //{
        //    if (originalUrl == null)
        //        return null;

        //    // *** Absolute path - just return
        //    if (originalUrl.IndexOf("://") != -1)
        //        return originalUrl;

        //    // *** Fix up image path for ~ root app dir directory
        //    if (originalUrl.StartsWith("~"))
        //    {
        //        string newUrl = "";
        //        if (HttpContext.Current != null)
        //            newUrl = HttpContext.Current.Request.ApplicationPath +
        //                  originalUrl.Substring(1).Replace("//", "/");
        //        else
        //            // *** Not context: assume current directory is the base directory
        //            throw new ArgumentException("Invalid URL: Relative URL not allowed.");

        //        // *** Just to be sure fix up any double slashes
        //        return newUrl;
        //    }

        //    return originalUrl;
        //}

        //public static string ResolveServerUrl(string serverUrl, bool forceHttps)
        //{
        //    // *** Is it already an absolute Url?
        //    if (serverUrl.IndexOf("://") > -1)
        //        return serverUrl;

        //    // *** Start by fixing up the Url an Application relative Url
        //    string newUrl = ResolveUrl(serverUrl);

        //    Uri originalUri = HttpContext.Current.Request.Url;
        //    newUrl = (forceHttps ? "https" : originalUri.Scheme) +
        //             "://" + originalUri.Authority + newUrl;

        //    return newUrl;
        //}

        ///// <summary>
        ///// This method returns a fully qualified absolute server Url which includes
        ///// the protocol, server, port in addition to the server relative Url.
        ///// 
        ///// It work like Page.ResolveUrl, but adds these to the beginning.
        ///// This method is useful for generating Urls for AJAX methods
        ///// </summary>
        ///// <param name="ServerUrl">Any Url, either App relative or fully qualified</param>
        ///// <returns></returns>
        //public static string ResolveServerUrl(string serverUrl)
        //{
        //    return ResolveServerUrl(serverUrl, false);
        //}

    }
}
