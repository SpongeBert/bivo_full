<%@ Page Language="C#" MasterPageFile="~/mainSC.master" AutoEventWireup="true" CodeFile="Register.aspx.cs"
    Inherits="NopSolutions.NopCommerce.Web.RegisterPage" %>

<%@ Register TagPrefix="nopCommerce" TagName="CustomerRegister" Src="~/Modules/CustomerRegister.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" runat="Server">
    <nopCommerce:CustomerRegister ID="ctrlCustomerRegister" runat="server" />
</asp:Content>
