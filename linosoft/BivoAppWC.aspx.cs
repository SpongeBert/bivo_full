﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;

public partial class women : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        FillAge();
        FillDdlHeight1();
        FillDdlWeight1();
        FillDdlHeight21();
        FillDdlHeight22();
        FillDdlWeight2();
        FillAge2();
    }
    private void FillAge()
    {

        //DdlAge.Items.Clear();
        int len = 101;
        for (int i = 16; i < len; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlAge.Items.Add(li);
        }
    }
      private void FillDdlWeight1()
    {
        //DdlWeight1.Items.Clear();

        int len = 166;//141;
        for (int i = 45; i < len; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlWeight1.Items.Add(li);
        }
    }
    private void FillDdlHeight1()
    {

        //DdlHeight1.Items.Clear();
        int len = 216;
        for (int i = 150; i < len; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlHeight1.Items.Add(li);
        }
    }
    private void FillAge2()
    {

        //DdlAge2.Items.Clear();
        int len = 101;
        for (int i = 16; i < len; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlAge2.Items.Add(li);
        }
    }
    private void FillDdlHeight21()
    {
        //DdlHeight21.Items.Clear();
        //DdlHeight21.Items.Clear();
        int len = 8;
        for (int i = 4; i < len; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlHeight21.Items.Add(li);
        }
    }
    private void FillDdlHeight22()
    {

        //DdlHeight22.Items.Clear();
        decimal len = 12m;
        for (decimal i = 0m; i < len; i += 0.5m)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlHeight22.Items.Add(li);
          //  DdlHeight22.SelectedIndex = 16;
        }

    }
    private void FillDdlWeight2()
    {

        //DdlWeight2.Items.Clear();
        int len = 365;// 308;
        for (int i = 99; i < len; i++)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            DdlWeight2.Items.Add(li);
        }
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        if (metric2.Visible)
        {
            metric2.Visible = false;
            form1.Visible = true;
            //Response.Redirect("./women.aspx?gen=2&cm=1");
        }
        else
        {
            form1.Visible = false;
            metric2.Visible = true;
           // Response.Redirect("./women.aspx?gen=2&cm=2");
        }
    }
    
    protected void Button1_Click(object sender, EventArgs e)
    {
    string VisitorsIPAddr = string.Empty;
    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
    {
        VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
    }
    else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
    {
        VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
    }
      string gender = "W";
      string cm = "2";
      string  collar = "0";
      string height = DdlHeight21.SelectedValue + "'" + DdlHeight22.SelectedValue;
      string weight = DdlWeight2.SelectedValue;
      string age = DdlAge2.SelectedValue;
      string FCup=DdlCupsize21.SelectedValue + "-" + DdlCupsize22.SelectedValue;
      string FArms=ddlArms2.SelectedValue;
      string FWaist=ddlWaist2.SelectedValue;
      string FHips=ddlHips2.SelectedValue;
        com.bivolino.biometric.BioApp ba = new com.bivolino.biometric.BioApp();
        com.bivolino.biometric.Antwoord antwoord = ba.CalculateSize(gender, cm, age, weight, height, collar, Fit.Value, VisitorsIPAddr, FCup, FArms, FWaist, FHips);
        string p1 = antwoord.Collarsize.ToString();
        string p2 = antwoord.Bmi.ToString();
        string p3 = antwoord.SizePredicition.ToString();
        string p4 = antwoord.Linosoft.ToString();
        string p5 = antwoord.Gender.ToString();
       Response.Redirect("./resultc.aspx?p1=" + p1 + "&p2=" + p2 + "&p3=" + p3 + "&p4=" + p4 + "&p5=" + p5);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
    string VisitorsIPAddr = string.Empty;
    if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
    {
        VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
    }
    else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
    {
        VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
    }
      string gender = "W";
      string cm = "1";
      string  collar = "0";
      string height = DdlHeight1.SelectedValue;
      string weight = DdlWeight1.SelectedValue;
      string age = DdlAge.SelectedValue;
      string FCup=DdlCupsize11.SelectedValue + "-" + DdlCupsize12.SelectedValue;
      string FArms=ddlArms.SelectedValue;
      string FWaist=ddlWaist.SelectedValue;
      string FHips=ddlHips.SelectedValue;
        com.bivolino.biometric.BioApp ba = new com.bivolino.biometric.BioApp();
        com.bivolino.biometric.Antwoord antwoord = ba.CalculateSize(gender, cm, age, weight, height, collar, Fit.Value, VisitorsIPAddr, FCup, FArms, FWaist, FHips);
        string p1 = antwoord.Collarsize.ToString();
        string p2 = antwoord.Bmi.ToString();
        string p3 = antwoord.SizePredicition.ToString();
        string p4 = antwoord.Linosoft.ToString();
        string p5 = antwoord.Gender.ToString();
       Response.Redirect("./resultc.aspx?p1=" + p1 + "&p2=" + p2 + "&p3=" + p3 + "&p4=" + p4 + "&p5=" + p5);
  }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("./BivoAppMC.aspx");
    }
}