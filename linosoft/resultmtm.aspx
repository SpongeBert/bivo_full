﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="resultmtm.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="http://www.bivolino.com/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://www.bivolino.com/assets/css/style.css">
<style type="text/css">
 body{background-image: url('images/bg_home.jpg');}
 .wrapper{background:none;}
.progress-sm{height:40px;}
        .style22
        {
            font-size:22px;
            color: red;
            border:None;
            display:none;
        }
h3.heading-xs{font-weight: 600;}
h3.heading-xs {font-size: 15px;font-weight: 600;position: relative;top: 30px;z-index: 99;padding-left: 3%; padding-right:0.5%;BackColor="#FF6600"} 

@media (min-width: 992px){
.col-md-9 {
    width: 100%;
}
}

#mtmshirt_btn
{
	padding: 8px 28px;
	margin: 3px 4px;
	display: inline-block;
	color: #000000;
	font-size: 18px;
	font-weight:bold;
	cursor: pointer;
	background: #FF6600;
	background: linear-gradient(top, #FF6600 0%, #e64d00 100%);
	background: -moz-linear-gradient(top, #FF6600 0%, #e64d00 100%);
	background: -webkit-linear-gradient(top, #FF6600 0%, #e64d00 100%);
	background: -o-linear-gradient(top, #FF6600 0%, #e64d00 100%);
	border: 1px solid #C2C4C7;
	border-radius: 5px;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	-o-border-radius: 5px;
	box-shadow:0px 0px 2px 1px rgba(250, 245, 245, 0.25), inset 1px 1px 0px 0px rgba(255, 255, 255, 0.25);
	-moz-box-shadow:0px 0px 2px 1px rgba(250, 245, 245, 0.25), inset 1px 1px 0px 0px rgba(255, 255, 255, 0.25);
	-webkit-box-shadow:0px 0px 2px 1px rgba(250, 245, 245, 0.25), inset 1px 1px 0px 0px rgba(255, 255, 255, 0.25);
	-o-box-shadow:0px 0px 2px 1px rgba(250, 245, 245, 0.25), inset 1px 1px 0px 0px rgba(255, 255, 255, 0.25);
	text-shadow: 1px 1px 0px rgba(255, 255, 255, 0.50);
}
#mtmshirt_btn:hover {
	background: linear-gradient(top, #ff801a 0%, #FF6600 100%);
	background: -moz-linear-gradient(top, #ff801a 0%, #FF6600 100%);
	background: -webkit-linear-gradient(top, #ff801a 0%, #FF6600 100%);
	background: -o-linear-gradient(top, #ff801a 0%, #FF6600 100%);
	text-decoration: none !important;
}
#mtmshirt_btn:active{
	opacity:0.8;
}
    }
</style>
    
<script type="text/javascript">
	function openInParentWindow(url) {
		self.opener.location = url;
		return false;
	}
	</script> 
</head>
<body>
    <form id="form1" runat="server" visible="true">
<div style="position: absolute; top: 10px; left: 10px;">  
    <asp:TextBox ID="resSizePrediction" runat="server" Font-Size="X-Large" columns="80" style="border:None" Width="98%" Height="40"></asp:TextBox>
</div>
</br>
</br>
<div style="position: absolute; top: 40px; left: 10px;">  
    <asp:TextBox ID="resSizePrediction2" runat="server" columns="80" Width="98%" Height="40" style="border:None"></asp:TextBox>
</div>
</br>
</br>
</br>

<div class="wrapper" style="width:100%;">
<div class="container content" style="width:70%;float:left;">     
        <div class="row" style="margin-top:-30px;">
     <div class="col-md-9">
     <div class="tab-v2">
                              
                    <div class="tab-content">
                    
                        <div class="tab-pane fade in active" id="tab-1">
       <div class="row">
 
                             <div class="col-md-6" style="width:90%;">
                                    <h3 class="heading-xs"><%=Request.QueryString["p1"]%> <span class="pull-right"><%=Request.QueryString["p4"]%>%</span></h3>
                                    <div class="progress progress-u progress-sm">
                                        <div class="progress-bar progress-bar-u" role="progressbar" aria-valuenow="<%=Request.QueryString["p4"]%>" aria-valuemin="0" aria-valuemax="100" style="width: <%=Request.QueryString["p4"]%>%">
                                        </div>
                                    </div>

                                    <h3 class="heading-xs"><%=Request.QueryString["p3"]%> <span class="pull-right"><%=Request.QueryString["p5"]%>%</span></h3>
                                    <div class="progress progress-u progress-sm">
                                        <div class="progress-bar progress-bar-u" role="progressbar" aria-valuenow="<%=Request.QueryString["p5"]%>" aria-valuemin="0" aria-valuemax="100" style="width: <%=Request.QueryString["p5"]%>%">
                                        </div>
                                    </div>

                                    
                                </div>
                                <!-- End Small Progress Bar -->
          </div>       
     </div> 
      </div>       
     </div>  
    </div>
    
</div><!--/End Wrapepr-->


</div>
</div>

<div class="style22" style="position: absolute; top: 90px; left: 100px;border:None;">  
 <asp:TextBox ID="resRood" runat="server" columns="80" style="border:None" Width="800" Height="40"></asp:TextBox>
</div>
<div style="position: absolute; top: 330px; left: 180px;">  
  <%--<asp:Button ID="Button2" runat="server"  Text="OVERHEMD OP MAAT" ForeColor="Black" BackColor="#FF6600" onclick="Button2_Click" style="width:200px;height:40px;font-size: 15px;font-weight: 600;" /> 
--%>
<a id="mtmshirt_btn" href="http://www.bivolino.com/nl/heren-overhemden/overhemden-op-maat.html" target="_parent" onclick="return(openInParentWindow(this.href));">OVERHEMD OP MAAT</a>
</div>
</form>  
</body>
</html>