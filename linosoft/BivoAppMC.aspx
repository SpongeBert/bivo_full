﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="BivoAppMC.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="StyleSheet.css" rel="stylesheet" type="text/css"  style ="width: 100%; height: 100%" media="all" rev="Stylesheet" />
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
    
    <style type="text/css">
        .style3
        {
            width: 424px;
            font-size:15px;
            font-weight:600;
        }
        .style4
        {
            width: 140px;
            font-size:17px;
            font-weight:600;
        }
        .style13
        {
            width: 224px;
            font-size: 15px;
            font-weight: 600;
        }
        
    </style>
</head>
<body>
    <form id="form1" runat="server" visible="true">
    <div>
         <asp:HiddenField ID="Fit" runat="server" Value="BIVOSHOP" />
         <asp:HiddenField ID="Email" runat="server" Value="1"  />
         <asp:HiddenField ID="FCup" runat="server" Value="1" />
         <asp:HiddenField ID="FArms" runat="server" Value="1" />
         <asp:HiddenField ID="FWaist" runat="server" Value="1" />
         <asp:HiddenField ID="FHips" runat="server" Value="1" />
   </div>
    <div style="text-align: left;padding-left:20px;padding-top:10px;">   
        <b><BIG>UW JUISTE MAAT EN FIT</BIG></b><br>
        <br /><b>Wij berekenen de perfecte fit gebaseerd op uw persoonlijke maten</b><br><br>
        <b><asp:LinkButton ID="LinkButton2" ForeColor="Black" runat="server" style="margin-left:20px;"><img alt="Maatvoorspeller" src="images/men_icon.png" style="width:50px;height:50px;" /></asp:LinkButton>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:LinkButton ID="LinkButton1" ForeColor="Black" runat="server" onclick="LinkButton1_Click"><img alt="Maatvoorspeller" src="images/women_icon.png" style="width:50px;height:50px;" /></asp:LinkButton>
        </b></div>
        <div style="text-align: left;margin-left:20px;">
        <br /><br />
          <u>  <asp:LinkButton ID="LinkButton3" ForeColor="Black" runat="server" onclick="LinkButton3_Click">Verander imperial waardes naar metrische waardes</asp:LinkButton></u>
            </b><br /><br />
      <table id="trMetric1" style="width:100%;">
           <B><tr>
                <td class="style4">Lengte</td>
                <td class="style3">
        <asp:DropDownList ID="DdlHeight1" runat="server"  Width="100">
        </asp:DropDownList>
                Cm</td>
            </tr>
            <tr>
                <td class="style4">Gewicht</td>
                <td class="style3">
                    <asp:DropDownList ID="DdlWeight1" runat="server" style="margin-left: 0px" Width="100">
        </asp:DropDownList>
                Kg</td>
            </tr>
            <tr>
            <td class="style4">Boordmaat</td>
                
             <td class="style3">
                    <asp:DropDownList ID="DdlCollarsize1" runat="server" Width="100"> </asp:DropDownList>
                Cm</td>
            </tr>
            <tr>
                <td class="style4">Leeftijd</td>
                <td class="style3">
                    <asp:DropDownList ID="DdlAge" runat="server"  Width="100"></asp:DropDownList>
                    Years</td>
                   </tr></b>
                </table><br>
  <asp:Button ID="Button1" runat="server" style="text-align:center;width:150px;height:40px;font-weight:600;margin:30px 0 0 50px;" 
        Text="MIJN MAAT IS ?" ForeColor="Black" BackColor="#FF6600" 
            onclick="Button1_Click" /></center> 
       </form>
    <form id="metric2" visible="false"  runat="server">
  <div style="text-align: left;padding-left:20px;padding-top:10px;"> 
        <img alt="" src="rightsize.png" style="width: 550px; height: 87px" /><br />
        &nbsp;&nbsp;&nbsp;         
        &nbsp;<br />
        <b><asp:LinkButton ID="LinkButton4" ForeColor="Black" runat="server" style="margin-left:20px;"><img alt="size predictor men" src="images/men_icon.png" style="width:50px;height:50px;" /></asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:LinkButton ID="LinkButton5" ForeColor="Black" runat="server" onclick="LinkButton1_Click"><img alt="size predictor men" src="images/women_icon.png" style="width:50px;height:50px;" /></asp:LinkButton>
       
       </div>
        <div style="text-align: left;margin-left:20px;">
            <br /><br />
           <u> <asp:LinkButton ID="LinkButton6" ForeColor="Black" runat="server" onclick="LinkButton3_Click">Imperial sizes to Metric sizes</asp:LinkButton></u>
            <br /></b><br />
      <table id="trMetric2" style="width:100%;" visible="false">
            
      <table id="trMetric2" style="width:100%; height: 98px;" visible="false">
            <b><tr>
                <td class="style4" rowspan="2">My Height</td>
                <td class="style13">
        <asp:DropDownList ID="DdlHeight21" runat="server" Width="100">
        </asp:DropDownList>
                Feet</td>
            </tr>
             <tr>
                <td class="style13">
        <asp:DropDownList ID="DdlHeight22" runat="server" Width="100">
        </asp:DropDownList>
                Inches</td>
            </tr>
            <tr>
                <td class="style4">My Weight</td>
                 
                 <td class="style13">
                    <asp:DropDownList ID="DdlWeight2" runat="server" style="margin-left: 0px" Width="100px"/>Lbs/stone<br />
                 </td>
                </tr>
                <tr>
                <td class="style4">My Age</td>
                  <td class="style3">
                    <asp:DropDownList ID="DdlAge2" runat="server"  Width="100px"/>Years</td>
                    
                </tr>
                 <tr>
                    <td class="style4">
            My Collarsize
            <td class="style13"><asp:DropDownList ID="DdlCollarsize2" runat="server" Width="100">
<asp:ListItem Text="14/XS" Value="14"></asp:ListItem><asp:ListItem Text="14.5" Value="14.5"></asp:ListItem><asp:ListItem Text="15/S" Value="15"></asp:ListItem><asp:ListItem Text="15.5" Value="15.5"></asp:ListItem><asp:ListItem Text="15.75/M" Value="15.75"></asp:ListItem><asp:ListItem Text="16" Value="16"></asp:ListItem><asp:ListItem Text="16.5/L" Value="16.5"></asp:ListItem><asp:ListItem Text="17" Value="17"></asp:ListItem><asp:ListItem Text="17.5/XL" Value="17.5"></asp:ListItem><asp:ListItem Text="17.75" Value="17.75"></asp:ListItem><asp:ListItem Text="18/2XL" Value="18"></asp:ListItem><asp:ListItem Text="18.5" Value="18.5"></asp:ListItem><asp:ListItem Text="19/3XL" Value="19"></asp:ListItem><asp:ListItem Text="19.25" Value="19.25"></asp:ListItem><asp:ListItem Text="20/4XL" Value="20"></asp:ListItem><asp:ListItem Text="20.5" Value="20.5"></asp:ListItem><asp:ListItem Text="21/5XL" Value="21"></asp:ListItem><asp:ListItem Text="21.25" Value="21.25"></asp:ListItem><asp:ListItem Text="21.75/6XL" Value="21.75"></asp:ListItem><asp:ListItem Text="22" Value="22"></asp:ListItem><asp:ListItem Text="22.5/7XL" Value="22.5"></asp:ListItem><asp:ListItem Text="23" Value="23"></asp:ListItem><asp:ListItem Text="23.25/8XL" Value="23.25"></asp:ListItem><asp:ListItem Text="23.5" Value="23.5"></asp:ListItem><asp:ListItem Text="23.75/9XL" Value="23.75"></asp:ListItem></asp:DropDownList>Inches</td>
</td></tr></b>
</table></br>
<asp:Button 
            ID="Button2" runat="server" style="text-align:center;width:150px;height:40px;font-weight:600;margin:30px 0 0 50px;" 
        Text="FIND MY SIZE" ForeColor="Black" BackColor="#FF6600" 
            onclick="Button2_Click" /></form>
      
  </body>
</html>