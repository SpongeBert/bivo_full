// All Rights Reserved.
// 
// Contributor(s): _______. 
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Autofac;
using Nop.Core.Data;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using System.Data.Entity.Core.Objects;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.SqlClient;


namespace NopSolutions.NopCommerce.Web
{
public partial class configPage : Page
{

 
public void Page_Load()
{
string[] stringArray = new string[1000];
int i=0;
var dataSettingsManager1 = new DataSettingsManager();
var dataProviderSettings1 = dataSettingsManager1.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings.txt"));
System.Data.SqlClient.SqlConnection con1 = new System.Data.SqlClient.SqlConnection(dataProviderSettings1.DataConnectionString);
 	using (con1)
    {
	con1.Open();
                
			
    try
    {
	System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(@"SELECT Sku as sku FROM Product where Deleted=0 and Sku!='' ORDER BY CreatedOnUtc DESC", con1);
	using (System.Data.SqlClient.SqlDataReader dr = command.ExecuteReader())
    {
	while (dr.Read())
    	{
         	stringArray[i] = dr["sku"].ToString();
			i++;
        }
                      
    }
	}
	finally {  }
    }

 			var dataSettingsManager = new DataSettingsManager();
            var dataProviderSettings = dataSettingsManager.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings2.txt"));

            
       
            System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(dataProviderSettings.DataConnectionString);
         

				con.Open();
    		int fabric_id = 0;
	   		int contrast_id=0;
			for(int j=0;j < stringArray.Length;j++)
			{
                try
                {
				
                    System.Data.SqlClient.SqlCommand command1 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select cast((xmldoc.query('/order/fab_/text()')) as varchar(max)))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr1 = command1.ExecuteReader())
                    {
                        if (dr1.Read()!=null)
                        {
                            fabric_id = int.Parse(dr1["FabID"].ToString());

                        }

                        
                    }
 					

				}

              

           		catch {  }

				try
                {
				
                    System.Data.SqlClient.SqlCommand command3 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select '1'+SUBSTRING (cast((xmldoc.query('/order/con_61/text()')) as varchar(max)),3,4))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr3 = command3.ExecuteReader())
                    {
                        if (dr3.Read()!=null)
                        {
                            contrast_id = int.Parse(dr3["FabID"].ToString());

                        }   
                    }
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command4 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select '1'+SUBSTRING (cast((xmldoc.query('/order/con_62/text()')) as varchar(max)),3,4))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr4 = command4.ExecuteReader())
                    {
                        if (dr4.Read()!=null)
                        {
                            contrast_id = int.Parse(dr4["FabID"].ToString());

                        }   
                    }
				}

				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command5 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select '1'+SUBSTRING (cast((xmldoc.query('/order/con_58/text()')) as varchar(max)),3,4))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr5 = command5.ExecuteReader())
                    {
                        if (dr5.Read()!=null)
                        {
                            contrast_id = int.Parse(dr5["FabID"].ToString());

                        }   
                    }
				}
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command6 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select '1'+SUBSTRING (cast((xmldoc.query('/order/con_87/text()')) as varchar(max)),3,4))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr6 = command6.ExecuteReader())
                    {
                        if (dr6.Read()!=null)
                        {
                            contrast_id = int.Parse(dr6["FabID"].ToString());

                        }   
                    }
				}
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command7 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select '1'+SUBSTRING (cast((xmldoc.query('/order/con_54/text()')) as varchar(max)),3,4))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr7 = command7.ExecuteReader())
                    {
                        if (dr7.Read()!=null)
                        {
                            contrast_id = int.Parse(dr7["FabID"].ToString());

                        }   
                    }
				}
				if(contrast_id==1)
				{
							System.Data.SqlClient.SqlCommand command8 = new System.Data.SqlClient.SqlCommand(@"select F.fabric_id as FabID
                            FROM Sel_Selectors  AS GAL 
                            INNER JOIN [Conf_Fabrics] as CF ON CF.conf_fabric_id = (select '1'+SUBSTRING (cast((xmldoc.query('/order/con_91/text()')) as varchar(max)),3,4))
                            INNER JOIN [Base_Fabrics] AS F on F.fabric_id = CF.fabrics_id
                            WHERE  (GAL.selector_id =  '" + stringArray[j] + "' ) ", con);
                    using (System.Data.SqlClient.SqlDataReader dr8 = command8.ExecuteReader())
                    {
                        if (dr8.Read()!=null)
                        {
                            contrast_id = int.Parse(dr8["FabID"].ToString());

                        }   
                    }
				}


 					

				}

              

           catch {  }
                
                
				float val = 100;
                float val1=100; 
                try
                {
                    System.Data.SqlClient.SqlCommand command10 = new System.Data.SqlClient.SqlCommand(@"SELECT TOP (1) PhysicalStock as stock FROM OFMS_FabricStock WHERE (fabric_id = " + fabric_id + ") AND (ActionID <> 1) ORDER BY Date DESC", con);

                    
                    using (System.Data.SqlClient.SqlDataReader dr10 = command10.ExecuteReader())
                    {

                        if (dr10.Read())
                        {
                            val = float.Parse(dr10["stock"].ToString());
                        }
						
                      
                    }
				
			    }
				catch{}

             
                try
                {
                    System.Data.SqlClient.SqlCommand command11 = new System.Data.SqlClient.SqlCommand(@"SELECT TOP (1) PhysicalStock as stock FROM OFMS_FabricStock WHERE (fabric_id = " + contrast_id + ") AND (ActionID <> 1) ORDER BY Date DESC", con);

                    
                    using (System.Data.SqlClient.SqlDataReader dr11 = command11.ExecuteReader())
                    {

                        if (dr11.Read())
                        {
                            val1 = float.Parse(dr11["stock"].ToString());
                        }
						
                      
                    }
				
			    }
				catch{}

				try{
				var dataSettingsManager2 = new DataSettingsManager();
				var dataProviderSettings2 = dataSettingsManager1.LoadSettings(HttpContext.Current.Server.MapPath("~/App_Data/Settings.txt"));
				System.Data.SqlClient.SqlConnection con2 = new System.Data.SqlClient.SqlConnection(dataProviderSettings2.DataConnectionString);
 				using (con2)
    			{
				con2.Open();
				if((val <= 2.5) || (val1 <= 2.5))
						{
	              
					
					//Response.Redirect("http://www.bivolino.com/Mobile/config2.aspx?CID=116&cur=1&x=" + stringArray[j]);
				 	string Sku= stringArray[j];
					System.Data.SqlClient.SqlCommand command12 = new System.Data.SqlClient.SqlCommand(@"UPDATE [bivoshop2018].[dbo].[Product] SET Published=0,StockQuantity=0 WHERE Sku =@Sku",con2);
                
                  	command12.Parameters.AddWithValue("@Sku", Sku);
                    command12.ExecuteNonQuery();
					
	
					}
				else if((val > 2.5) || (val1 > 2.5))
						{
	              
					
					//Response.Redirect("http://www.bivolino.com/Mobile/config2.aspx?CID=116&cur=1&x=" + stringArray[j]);
				 	string Sku= stringArray[j];
					System.Data.SqlClient.SqlCommand command13 = new System.Data.SqlClient.SqlCommand(@"UPDATE [bivoshop2018].[dbo].[Product] SET Published=1,StockQuantity=100 WHERE Sku =@Sku",con2);
                
                  	command13.Parameters.AddWithValue("@Sku", Sku);
                    command13.ExecuteNonQuery();
					
	
					}
					}
 					}
				catch {}
              

                }

Response.Redirect("https://www.bivolino.com/nl/"); 

                            
}

}
}
